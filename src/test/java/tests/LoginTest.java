package tests;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import tests.common.BaseTest;
import utils.DriverHelper;

public class LoginTest extends BaseTest {



    @Test
    public void loginWithValidParametersTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        DriverHelper.openPage("admin");

        Assert.assertTrue("Login page ist`n shown ", loginPage.waitForPageLoad());

        loginPage
                .writeEmail("attentivetester@gmail.com")
                .clickButton();

        Assert.assertTrue("Password page isn`t showen",loginPage.waitForPageLoad());

        loginPage
                .writePassword("78qa22!#")
                .clickButton();

        Thread.sleep(10000);
    }

}
