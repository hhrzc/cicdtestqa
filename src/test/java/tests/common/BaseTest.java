package tests.common;

import common.Base;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import utils.DriverHelper;
import utils.Utils;
import utils.listeners.ScreenshotListener;
import utils.listeners.SkipTestDependsOnBuild;
import utils.listeners.TestListener;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Listeners({ScreenshotListener.class, TestListener.class, SkipTestDependsOnBuild.class})
public class BaseTest extends Base {

    private static ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();
    private final String GRID_URL = "";
    private Map<String, Object> capabilities = new HashMap<>();
    private ChromeOptions chromeOptions = new ChromeOptions();
    private FirefoxOptions fireFoxOptions = new FirefoxOptions();
    private URL host;

    public static WebDriver getDriver() {
        return driverPool.get();
    }

    private void initHost() {
        host = null;
        try {
            host = new URL(GRID_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void initChromeDriver(ChromeOptions newOptions) {
        if (Utils.isEnvironmentRemote()) {
            initHost();
            RemoteWebDriver driver = new RemoteWebDriver(host, newOptions);
            driver.setFileDetector(new LocalFileDetector());
            driverPool.set(driver);
        } else {
            System.setProperty("webdriver.chrome.driver",
                    new File(this.getClass().getResource("/chromedriver.exe").getFile())
                            .getPath());
            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NONE);
            driverPool.set(new ChromeDriver(newOptions));
        }
    }

    private void initFireFoxDriver(FirefoxOptions newOptions) {
        if (Utils.isEnvironmentRemote()) {
            initHost();
            RemoteWebDriver driver = new RemoteWebDriver(host, newOptions);
            driver.setFileDetector(new LocalFileDetector());
            driverPool.set(driver);
        } else {
            System.setProperty("webdriver.gecko.driver",
                    new File(this.getClass().getResource("/geckodriver.exe").getFile())
                            .getPath());
            fireFoxOptions.setPageLoadStrategy(PageLoadStrategy.NONE);
            driverPool.set(new FirefoxDriver((newOptions)));
        }
    }

    public ChromeOptions getChromeOptions() {
        if (!capabilities.isEmpty()) {
            capabilities.forEach((k, v) -> chromeOptions.setCapability(k, v));
        }
        return chromeOptions;
    }

    public FirefoxOptions getFireFoxOptions() {
        if (!capabilities.isEmpty()) {
            capabilities.forEach((k, v) -> fireFoxOptions.setCapability(k, v));
        }
        return fireFoxOptions;
    }

    public void setCapabilities(HashMap<String, Object> newCapabilities) {
        capabilities.putAll(newCapabilities);
    }

    public void clearCapabilities() {
        capabilities.clear();
    }

    @BeforeTest(alwaysRun = true)
    @Parameters("Browser")
    public void setUp(@Optional("chrome") String browser) {
        getChromeOptions().setCapability("seleniumProtocol", "WebDriver");
        for (Map.Entry entry : capabilities.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }
        if (browser.equals("chrome")) {
            initChromeDriver(getChromeOptions());
        } else if (browser.equals("firefox")) {
            initFireFoxDriver(getFireFoxOptions());
        }
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        if (Utils.isEnvironmentRemote()) {
            driver.manage().window().setSize(new Dimension(1920, 1080));
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void closeAllTabExceptFirst() {
        if (getDriver().getWindowHandles().size() > 1) {
            DriverHelper.closeAllTabsExceptFirst();
        }
    }

    @AfterTest(alwaysRun = true)
    public void quit() throws Exception {
        driverPool.get().close();
        driverPool.get().quit();
        long start = System.currentTimeMillis();
        long end = start + 10000;
        while (true) {
            if (getDriver().toString().contains("(null)")) {
                break;
            }
            if (System.currentTimeMillis() > end) {
                throw new Exception("The driver is not closed during 10 seconds");
            }
        }
    }
}

