package common;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.common.BasePage;
import utils.DriverHelper;
import utils.Utils;
import utils.log.Log;

import java.util.function.Supplier;

public abstract class Base {

    @Attachment(type = "text/plain")
    public String saveOutput(String data) { return data;  }

    @Attachment(value = "Output - {outPutName}", type = "text/plain")
    public String saveOutput(String outPutName, String data) {
        return data;
    }

    @Attachment(value = "Page screenshot - {screenshotName}", type = "image/png")
    public byte[] saveScreenshot(String screenshotName) {
        return ((TakesScreenshot) DriverHelper.getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    protected <T extends BasePage> T repeatIfFails(Supplier<T> action, int duration){
        return repeatIfFails(action, duration, "tests");
    }

    protected <T extends BasePage> T repeatIfFails(Supplier<T> action, int durationInMilliseconds, String methodName){
        try{
            return action.get();
        } catch (Exception e){
            Log.info(String.format("The %s failed, tries again....", methodName));
            Log.info(e.getMessage() != null ? e.getMessage() : e.toString());
            Utils.delayMillisec(durationInMilliseconds);
            return action.get();
        }
    }

    protected <T> T returnOrThrowExceptionIfNull(T t){
        if(t == null){
            throw new NullPointerException();
        } else {
            return t;
        }
    }

}
