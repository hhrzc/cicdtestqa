package utils;

import io.github.sukgu.Shadow;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.listeners.ScreenshotListener;
import utils.log.Log;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import tests.common.BaseTest;

import static java.util.stream.IntStream.range;

public class DriverHelper implements SearchContext {

    public static final int TIMEOUT_DEFAULT = 15;
    public static final int TIMEOUT_EXTRA_LONG = 60;
    public static final int TIMEOUT_LONG = 30;
    public static final int TIMEOUT_SHORT = 5;
    public static final int TIMEOUT_MINIMAL = 2;

    public static final String HOME_URL = System.getProperty("home.page.url") == null ?
            "https://marinacsecond-store.myshopify.com/" : System.getProperty("home.page.url");

    public static final String JS_HOVER_SCRIPT = "var evObj = document.createEvent('MouseEvents');" +
            "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, " +
            "null);" +
            "arguments[0].dispatchEvent(evObj);";
    private static final String MENU_XPATH_SELECTOR = "//div[contains(@class, 'menu')]";
    private final SearchContext context;
    private final WebDriverWait wait;
    private final JavascriptExecutor js;

    public DriverHelper(SearchContext context, int timeoutSec) {
        this.context = context;
        this.wait = new WebDriverWait(getDriver(), timeoutSec);
        js = (JavascriptExecutor) getDriver();
    }

    public DriverHelper() {
        this(TIMEOUT_DEFAULT);
    }

    public DriverHelper(int timeoutSec) {
        this(getDriver(), timeoutSec);
    }

    public static WebDriver getDriver() {
        return BaseTest.getDriver();
    }

    public static void waitForPageLoad() {
        WebDriverWait w = new WebDriverWait(getDriver(), TIMEOUT_LONG);
        w.until(driver -> String
                .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                .equals("complete"));
        Log.info("Waiting via JS for the page loading");
    }

    @Step("Open URL - {url}")
    public static void openUrl(String url) {
        getDriver().get(url);
        waitForPageLoad();
        Log.info("The " + url + " is opened");
    }

    public static Boolean waitForPageUrlContainsText(String text) {
        try {
            WebDriverWait w = new WebDriverWait(getDriver(), TIMEOUT_SHORT);
            w.until(ExpectedConditions.urlContains(text));
            Log.info("The url with \""+text+"\" is loaded");
            return true;
        } catch (Exception e) {
            Log.error("The url doesn`t contains \"" + text + "\"");
            return false;
        }
    }

    @Step("Open page - {url}")
    public static void openPage(String url) {
        openUrl(HOME_URL + url);
        waitForPageLoad();
        Log.info("The " + HOME_URL + " is opened");
    }

    public static void openHomePage() {
        openUrl(HOME_URL);
        waitForPageLoad();
        Log.info("The Home page is opened");
    }

    @Step("Refresh the page and wait until page loaded")
    public static void refreshPageAndWaitUntilPageLoaded() {
        getDriver().navigate().refresh();
        waitForPageLoad();
        Log.info("The page is refreshed and loaded");
    }

    @Step("Refresh the page")
    public static void refreshPage() {
        getDriver().navigate().refresh();
        Log.info("The page is refreshed");
    }

    @Step("Refresh/ClearCache the page and wait until page loaded")
    public static void refreshPageAndClearCache() {
        Actions actions = new Actions(getDriver());
        actions.sendKeys(Keys.chord(Keys.SHIFT, Keys.F5)).build().perform();
        waitForPageLoad();
        Log.info("The page is refreshed and all cache is cleared");
    }


    public static void closeAllInactiveTabs() {
        WebDriver driver = getDriver();
        String activeTab = driver.getWindowHandle();
        driver.getWindowHandles().stream()
                .filter(t -> !t.equals(activeTab))
                .forEach(t -> driver.switchTo().window(t).close());
        driver.switchTo().window(activeTab);
        Log.info("All inactive tabs are closed");
    }

    public static void closeAllTabsExceptOriginal(String originalHandle) {
        for (String handle : getDriver().getWindowHandles()) {
            if (!handle.equals(originalHandle)) {
                getDriver().switchTo().window(handle);
                getDriver().close();
            }
        }
        getDriver().switchTo().window(originalHandle);
        Log.info("All inactive tabs are closed except original");
    }

    public static void openPageInNewTab(String url) {
        closeAllInactiveTabs();
        ((JavascriptExecutor) getDriver()).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        getDriver().get(url);
        Log.info("The " + url + " new page is opened in new tab");
    }

    public static void closeAllTabsExceptFirst() {
        ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(0));
        closeAllInactiveTabs();
        Log.info("All inactive tabs are closed except first");
    }

    public static String getPageNetworkEntriesJS() {
        String networkEntriesScript = "var performance = window.performance || window.mozPerformance || window" +
                ".msPerformance " +
                "|| window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";
        String result = ((JavascriptExecutor) getDriver()).executeScript(networkEntriesScript).toString();
        Log.info("Getting Page Network Entries via JS");
        return result;
    }

    public static ArrayList<String> parseNetworkEntries(String networkData) {
        String[] networkEntries = StringUtils.substringsBetween(networkData, "{", "},");
        Log.info("Parsing network data entries");
        return new ArrayList<>(Arrays.asList(networkEntries));
    }



    public static ArrayList<String> getLogEntryMessages() {
        ArrayList<String> logEntryMessages = new ArrayList<>();
        //log clear after call
        LogEntries logEntries = getDriver().manage().logs().get(LogType.PERFORMANCE);
        logEntries.forEach(t -> logEntryMessages.add(t.getMessage()));
        Log.info("Log Entry Messages are taken");
        return logEntryMessages;
    }

    @Step("Refresh the page Until The Maximum Number Of Report Msg Is Not Displayed")
    public void refreshPageUntilTheMaximumNumberOfReportMsgIsNotDisplayed(int maxTimesRefreshWithDelay) throws Exception {
        boolean isMessageNotDisplayed = false;
        DriverHelper.refreshPage();
        for (int iteration = 0; iteration < maxTimesRefreshWithDelay; iteration++) {
            if (isMaximumNumberOfReportAlertDisplayed()) {
                Utils.delay(1, 15);
                DriverHelper.refreshPage();
            } else {
                isMessageNotDisplayed = true;
                break;
            }
        }
        if (!isMessageNotDisplayed) {
            ScreenshotListener screenshotListener = new ScreenshotListener();
            screenshotListener.saveScreenshot();
            throw new Exception("The 'Maximum number of reports per minute reached.' is displayed");
        }
        Log.info("The page is refreshed until The Maximum Number Of Report Msg Is Not Displayed");
    }

    @Step("Is the 'Maximum number of reports per minute reached.' alert displayed")
    public Boolean isMaximumNumberOfReportAlertDisplayed() {
        return isAlertDisplayed("Maximum number of reports per minute reached.");
    }

    @Override
    public List<WebElement> findElements(By by) {
        List<WebElement> result = context.findElements(by);
        Log.info("The number of elements is for path" + by.toString() + result);
        return result;
    }

    @Override
    public WebElement findElement(By by) {
        return context.findElement(by);
    }

    public boolean isElementPresent(By locator) {
        try {
            WebElement element = getDriver().findElement(locator);
            element.getTagName();
            return true;
        } catch (WebDriverException ignored) {
            Log.error("The element with locator " + locator + " doesn't found");
            return false;
        }
    }

    public boolean isElementPresent(WebElement element) {
        try {
            element.getTagName();
            return true;
        } catch (WebDriverException ignored) {
            Log.error("The element " + element + " doesn't found");
            return false;
        }
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            return waitForVisibility(element).isDisplayed();
        } catch (WebDriverException ignored) {
            Log.error("The element " + element + " doesn't displayed");
            return false;
        }
    }

    public boolean isNotElementDisplayed(WebElement element) {
        try {
            return !waitForVisibility(element).isDisplayed();
        } catch (WebDriverException ignored) {
            Log.error("The element " + element + " is displayed but shouldn't");
            return false;
        }
    }


    public boolean isElementDisplayed(By locator) {
        try {
            return waitForVisibility(locator).isDisplayed();
        } catch (WebDriverException ignored) {
            Log.error("The element with locator " + locator + " doesn't displayed");
            return false;
        }
    }

    public WebElement waitForDisplayed(WebElement element) {
        try {
            wait.until(driver -> isElementDisplayed(element));
        } catch (Exception e) {
            Log.error("The element " + element + " doesn't displayed");
            throw new ElementNotVisibleException("WebElement " + element + " doesn't displayed");
        }
        return element;
    }

    public List<WebElement> waitForDisplayed(List<WebElement> element) {
        wait.until(driver -> areAllElementDisplayed(element));
        Log.info("List of element is present.");
        return element;
    }

    public WebElement waitForPresence(By locator) {
        WebElement result = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        Log.info("Element " + locator + " is present.");
        return result;
    }

    public WebElement waitForPresence(WebElement element) {
        wait.until(driver -> isElementPresent(element));
        Log.info("Element " + element + " is present.");
        return element;
    }

    public void waitForAbsence(WebElement element) {
        Log.info("Wait for Absence element " + element);
        wait.until(driver -> !isElementPresent(element));
    }

    public void waitForAbsence(By locator) {
        Log.info("Wait for Absence element by locator " + locator);
        try {
            wait.until(driver -> !isElementPresent(locator));
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            wait.until(driver -> !isElementPresent(locator));
        }
    }

    public boolean isElementAbsence(By locator) {
        return wait.until(driver -> !isElementPresent(locator));
    }

    public boolean isElementAbsence(WebElement element) {
        return wait.until(driver -> !isElementPresent(element));
    }

    public void waitForSelected(WebElement element) {
        wait.until(driver -> element.isSelected());
        Log.info("Wait until element " + element + "doesn`t selected");
    }

    public boolean isElementSelected(By locator) {
        WebElement element = getDriver().findElement(locator);
        return element.isSelected();
    }

    public boolean isElementSelected(WebElement element) {
        return element.isSelected();
    }

    public void waitForNotSelected(WebElement element) {
        Log.info("Wait until element " + element + "doesn`t selected");
        wait.until(driver -> !element.isSelected());

    }

    public void waitAndSwitchIframe(String iframeID) {
        Log.info("Wait and switch iframe to " + iframeID);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id(iframeID)));
    }

    public WebElement waitForVisibility(WebElement element) {
        Log.info("Waiting until element " + element + "will visible");
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement waitForVisibility(By locator) {
        Log.info("Waiting until element by locator " + locator + "will visible");
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public WebElement waitForAttributeContains(WebElement element, String attribute, String expectedValue) {
        try {
            wait.until(ExpectedConditions.attributeContains(element, attribute, expectedValue));
        } catch (Exception e) {
            Log.error(String.format("Element '%s' isn't contains value '%s' in the attribute '%s'", element, expectedValue, attribute));
            throw new ElementNotVisibleException(
                    String.format("Element '%s' isn't contains value '%s' in the attribute '%s'.",
                            element, expectedValue, attribute));
        }
        return element;
    }

    public void waitForNumberOfElementsToBe(By locator, int number) {
        Log.info("Waiting for number of elements by locator " + locator + " " +number);
        wait.until(ExpectedConditions.numberOfElementsToBe(locator, number));

    }

    public void waitForElementAttributeContains(WebElement element, String attribute, String value) {
        wait.until(ExpectedConditions.attributeContains(element, attribute, value));
        Log.info("Attribute [" + attribute + "] of the element '" + element + "' contains value '" + value + "'");
    }

    public WebElement waitToBeClickable(WebElement element) {
        Log.info("Waiting until element " + element + "be clickable");
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement waitToBeClickable(By locator) {
        Log.info("Waiting until element by locator " + locator + "be clickable");
        return wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public WebElement waitAndClickWebElement(WebElement element) {
        waitToBeClickable(element).click();
        Log.info("Element " + element + " is clicked.");
        return element;
    }

    public String waitAndClickWebElementForByLocator(String locator) {
        try {
            waitToBeClickable(By.xpath(locator)).click();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            waitToBeClickable(By.xpath(locator)).click();
        }
        return locator;
    }

    public WebElement waitAndClickWebElement(By locator) {
        WebElement element = waitToBeClickable(locator);
        element.click();
        return element;
    }

    public List<WebElement> waitForVisibilityOfAllElements(List<WebElement> webElement) {
        return wait.until(ExpectedConditions.visibilityOfAllElements(webElement));
    }

    public List<WebElement> waitForAllElementsPresent(By by) {
        return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public boolean isInvisibilityAllElements(List<WebElement> webElement) {
        return wait.until(ExpectedConditions.invisibilityOfAllElements(webElement));
    }

    public WebElement scrollToElementJS(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        Log.info("Scrolling to the " + element);
        return element;
    }

    public void slowScrollToPageBottom(int delayMillisecondsBetweenPixels) {
        int contentHeight = ((Number) js.executeScript("return document.body.scrollHeight")).intValue();
        for (int pixel = 1; pixel <= contentHeight; pixel++) {
            js.executeScript(String.format("window.scrollTo(0, %s)", pixel));
            try {
                Thread.sleep(delayMillisecondsBetweenPixels);
            } catch (InterruptedException ignored) {

            }
        }
        Log.info("Scrolled to page bottom");
    }

    public WebElement hoverJS(WebElement element) {
        waitForPresence(element);
        js.executeScript(JS_HOVER_SCRIPT, element);
        Log.info("Hovering over the " + element);
        return element;
    }


    public WebElement clickJS(WebElement element) {
        waitToBeClickable(element);
        js.executeScript("arguments[0].click();", element);
        Log.info("The " + element + " is clicked");
        return element;
    }

    public WebElement clearAndTypeIntoInputField(WebElement inputElement, String value) {
        clearInputField(inputElement);
        if (value.length() > 0) {
            inputElement.sendKeys(value);
        }
        Log.info("The " + value + " is typed to " + inputElement);
        return inputElement;
    }

    public WebElement clearInputField(WebElement element) {
        waitAndClickWebElement(element);
        range(0, 10).forEach(value -> {
            element.sendKeys(Keys.DELETE);
            element.sendKeys(Keys.BACK_SPACE);
        });
        element.clear();
        if (Utils.isEnvironmentRemote()) {
            element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        } else {
            element.sendKeys(Keys.chord(String.valueOf('\u0001'), Keys.DELETE));
        }
        Log.info("The " + element + " is cleared");
        return element;
    }

    public WebElement clearInputFieldByKeys(WebElement element) {
        waitAndClickWebElement(element);
        range(0, 10).forEach(value -> {
            element.sendKeys(Keys.DELETE);
            element.sendKeys(Keys.BACK_SPACE);
        });
        if (Utils.isEnvironmentRemote()) {
            element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        } else {
            element.sendKeys(Keys.chord(String.valueOf('\u0001'), Keys.DELETE));
        }
        Log.info("The " + element + " is cleared by keys");
        return element;
    }

    @Step("Is the '{msg}' alert displayed")
    public boolean isAlertDisplayed(String msg) {
        try {
            String msgLocator = String.format("//div[contains(text(),'%s')]", msg);
            waitForVisibility(By.xpath(msgLocator));
            waitForAbsence(By.xpath(msgLocator));
            Log.info("The " + msg + " is displayed.");
            return true;
        } catch (Exception e) {
            Log.error("The " + msg + " isn't displayed.");
            return false;
        }
    }

    /**
     * Waits for appearing the pop-up menu
     *
     * @return the appeared menu
     */
    public WebElement waitForMenuToAppear() {
        return waitForVisibility(By.xpath(MENU_XPATH_SELECTOR));
    }

    /**
     * Waits for disappearing the pop-up menu
     */
    public void waitForMenuToDisappear() {
        waitForAbsence(By.cssSelector(MENU_XPATH_SELECTOR));
    }

    public void selectOptionFromDropdownMenu(String optionName) {
        waitForMenuToAppear();
        String locator = String.format("//div[contains(@class, 'menu')]" +
                "//div[contains(text(),'%s')]", optionName);
        scrollToElementJS(waitForPresence(By.xpath(locator)));
        waitAndClickWebElementForByLocator(locator);
        Log.info("The item with locator " + locator + " is selected from dropdown");
    }

    public void selectOptionFromDropdownMenuByMouseActions(String optionName) {
        waitForMenuToAppear();
        String locator = String.format("//div[contains(@class, 'menu')]" +
                "//div[contains(text(),'%s')]", optionName);
        WebElement option = waitForPresence(By.xpath(locator));
        scrollToElementJS(option);
        Utils.delay(0, 1);
        Actions actions = new Actions(DriverHelper.getDriver()).moveToElement(option)
                .pause(Duration.ofMillis(300))
                .click();
        actions.perform();
        Log.info("The item with locator " + locator + " is selected from dropdown by mouse actions");
    }

    /**
     * Closes the pop-up menu
     */
    public void closeMenuWithEsc() {
        new Actions(getDriver()).sendKeys(Keys.ESCAPE).perform();
        Log.info("The menu is closed by Esc");
    }

    public void closePopUpMenuWithEsc() {
        closeMenuWithEsc();
        waitForMenuToDisappear();
        Log.info("The popup is closed by Esc");
    }

    public String getElementClassName(WebElement element) {
        isElementDisplayed(element);
        return element.getAttribute("class");
    }

    public void waitForNumberOfWindowsToBe(int number) {
        wait.until(ExpectedConditions.numberOfWindowsToBe(number));
    }

    public void switchToTabWhoseUrlContains(String url) {
        Set<String> existingTabs = getDriver().getWindowHandles();
        for (String tab : existingTabs) {
            getDriver().switchTo().window(tab);
            if (getDriver().getCurrentUrl().contains(url)) {
                break;
            }
        }
        Log.info("Switching to tab with URL " + url);
    }

    public boolean isUrlContains(String value) {
        try {
            wait.until(ExpectedConditions.urlContains(value));
            return true;
        } catch (WebDriverException e) {
            Log.error("The URL doesn't contain " + value);
            return false;
        }
    }

    public boolean areAllElementDisplayed(List<WebElement> webElement) {
        try {
            waitForVisibilityOfAllElements(webElement);
            return true;
        } catch (WebDriverException e) {
            Log.error(webElement + "are not displayed");
            return false;
        }
    }

    public boolean isElementDisabled(WebElement element) {
        String isDisabled = waitForPresence(element).getAttribute("disabled");
        return (isDisabled != null && isDisabled.equalsIgnoreCase("true"));
    }


    public boolean isElementClickable(WebElement element) {
        try {
            waitToBeClickable(element);
            return true;
        } catch (WebDriverException e) {
            Log.error("Element " + element + " doesn`t clickable" );
            return false;
        }
    }

    public void navigateToElementAndClick(WebElement moveTo, WebElement clickOn) {
        new Actions(getDriver()).moveToElement(waitForPresence(moveTo))
                .pause(Duration.ofMillis(500))
                .click(waitToBeClickable(clickOn))
                .perform();
        Log.info("Moving to " + moveTo + " and clicking on " + clickOn);
    }
}
/**
    @Step("Get current URL")
    public static String getCurrentUrl() {
        String pageURL = getDriver().getCurrentUrl();
        Log.info("The current URL is " + pageURL);
        return pageURL;
    }
 */
/*
  public void waitForNumberOfElementsToBe(By locator, int number) {
    wait.until(ExpectedConditions.numberOfElementsToBe(locator, number));
  }

      public WebElement scrollToElementJS(By by) {
        WebElement element = waitForPresence(by);
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        return element;
    }

      public void waitForEnabled(WebElement element) {
    wait.until(driver -> element.isEnabled());
    }

    public void waitForNotEnabled(WebElement element) {
    wait.until(driver -> !element.isEnabled());
    }

     public static <T extends Function<? super WebDriver, V>, V> V waitForResult(T input,
                                                                                int secondsToTry) {
        WebDriverWait w = new WebDriverWait(getDriver(), secondsToTry);
        w.ignoring(WebDriverException.class).pollingEvery(Duration.ofSeconds(secondsToTry / 5));
        return w.until(driver -> input.apply(driver));


    public Object getBrowserUserAgent() {
        return js.executeScript("return navigator.userAgent;");
    }

    public WebDriverWait getWait() {
        return wait;
    }
        public WebElement waitForAttributeContains(By locator, String attribute, String expectedValue) {
        WebElement element = waitForVisibility(locator);
        try {
            wait.until(ExpectedConditions.attributeContains(locator, attribute, expectedValue));
        } catch (Exception e) {
            Log.info(String.format("Element '%s' isn't contains value '%s' in the attribute '%s'", element, expectedValue, attribute));
            throw new ElementNotVisibleException(
                    String.format("Element '%s' isn't contains value '%s' in the attribute '%s'.",
                            element, expectedValue, attribute));
        }
        return element;
    }

        public void scrollBy(int xOffset, int yOffset) {
        js.executeScript("window.scrollBy(arguments[0],arguments[1])", xOffset, yOffset);
    }

    public WebElement clickJS(By by) {
        WebElement element = waitForPresence(by);
        js.executeScript("arguments[0].click();", element);
        return element;
    }

    public void clearCookiesAndCached_NEW() {
        getDriver().get("chrome://settings/clearBrowserData");
        Utils.delayMillisec(5000);
        Actions actions = new Actions(getDriver());
        for (int i = 0; i < 7; i++) {
            actions.sendKeys(Keys.TAB).build().perform();
            Utils.delayMillisec(1000);
        }
        actions.sendKeys(Keys.ENTER).build().perform();
        Utils.delayMillisec(15000);
    }

    public void clearCookiesAndCached() {
        getDriver().get("chrome://settings/clearBrowserData");
        Utils.delayMillisec(5000);
        getDriver().switchTo().activeElement();
        getDriver().findElement(By.cssSelector("* /deep/ #clearBrowsingDataConfirm")).click();
        Utils.delayMillisec(5000);
    }

    public void clearCookiesAndCached_OLD() {
        Shadow shadow = new Shadow(getDriver());
        getDriver().get("chrome://settings/clearBrowserData");
        wait.until(driver -> isElementPresent(shadow.findElement("#clearBrowsingDataConfirm")));
        WebElement clearBrowser = (shadow.findElement("#clearBrowsingDataConfirm"));
        getDriver().findElement(By.xpath("//settings-ui")).sendKeys(Keys.ENTER);
        //shadow.findElement("#clearBrowsingDataConfirm").sendKeys(Keys.ENTER);
        //Utils.delay(0,8);
        waitForAbsence(clearBrowser);
        Log.info("The cookies and cache are cleared");
    }

        public void waitForSuccessMessageDisappear() {
        waitForPresence(By.xpath("//div/span[contains(text(), 'has been successfully')]"));
        waitForAbsence(By.xpath("//div/span[contains(text(), 'has been successfully')]"));
    }

        @Step("Wait for the '{msg}' alert displayed and disappeared")
    public void waitForAlertDisplayedAndDisappeared(String msg) {
        String msgLocator = String.format("//div[contains(text(),'%s')]", msg);
        waitForVisibility(By.xpath(msgLocator));
        waitForAbsence(By.xpath(msgLocator));
    }

    public void waitForHiddenOverlay() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[style*='position: fixed']")));
    }

        public void closeMenu() {
        getDriver().findElement(By.xpath("//div[contains(@style, 'z-index: 2000;')]")).click();
        waitForMenuToDisappear();
    }
        @Step("Test active menu for it's items existence")
    public boolean checkMenuOptions(List<String> expected) {
        List<WebElement> items = getDriver().findElements(By.cssSelector("[role='menu'] [role='menuitem'] > div >div > " +
                "div"));
        return items.stream().allMatch(act -> expected.stream()
                .anyMatch(act.getText()::equalsIgnoreCase));
    }
        public List<WebElement> getOptionsDropdownList() {
        waitForMenuToAppear();
        return getDriver().findElement(By.cssSelector(MENU_XPATH_SELECTOR))
                .findElements(By.cssSelector("span[role='menuitem']"));
    }

     public boolean waitForTextToBePresent(By locator, String value) {
        return wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, value));
    }

    public void waitForTextToBePresent(WebElement element, String value) {
        wait.until(ExpectedConditions.textToBePresentInElement(element, value));
    }

    public void waitForTextToBePresenceInValue(By locator, String value) {
        wait.until(ExpectedConditions.textToBePresentInElementValue(locator, value));
    }

    public void waitForTextToBePresenceInValue(WebElement element, String value) {
        wait.until(ExpectedConditions.textToBePresentInElementValue(element, value));
    }
        public boolean isElementClickable(By locator) {
        try {
            waitToBeClickable(locator);
            return true;
        } catch (WebDriverException e) {
            return false;
        }
    }
        public void enableImplicitWait(int timeoutSec) {
        getDriver().manage().timeouts().implicitlyWait(timeoutSec, TimeUnit.SECONDS);
    }

    public void disableImplicitWait() {
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    public void changeTheAttribute(WebElement webElement, String attr, String value) {
        js.executeScript("arguments[0].setAttribute(arguments[1],arguments[2]);", webElement, attr, value);
    }

    public void navigateToElement(WebElement webElement) {
        new Actions(getDriver()).moveToElement(waitForPresence(webElement)).perform();
    }
    }*/