package utils;

import com.amazonaws.util.IOUtils;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import pages.HomePage;
import pages.LoginPage;
import pages.common.BasePage;
import utils.data.UserData;
import utils.log.Log;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import static org.awaitility.Awaitility.await;

public final class Utils {

    private Utils() {

    }

    public static void delay(int minutes, int seconds) throws IllegalArgumentException {
        if (minutes < 0 || seconds < 0) {
            throw new IllegalArgumentException("Arguments cannot be negative!");
        }
        LocalDateTime timeLimit = LocalDateTime.now().plusMinutes(minutes);
        timeLimit = timeLimit.plusSeconds(seconds);
        while (LocalDateTime.now().isBefore(timeLimit)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }
    }

    public static boolean isEnvironmentRemote() {
        String env = System.getProperty("target.environment");
        return env != null && env.equals("remote");
    }

    public static void delayMillisec(int milliseconds){
        delay(0, milliseconds/1000);
    }
    public static boolean isProductionBuild() {
        String env = System.getProperty("home.page.url");
        return env != null && env.equals("https://www.cheq-platform.com/");
    }
    public <T extends BasePage> T writeAndClick(String s,T t){

        return t;
    }

/*
    public static HomePage logIn(UserData userData) {
        DriverHelper.openHomePage();
        HomePage homePage = new HomePage();
        LoginPage loginPage = new LoginPage();
        if (!loginPage.isPageLoginPage()
                && homePage.sideBarMenu.isCorrectUserLoggedIn(userData.getUsername())) {
            return homePage;
        } else {
            return new LoginPage().login(userData.getUsername(), userData.getPassword());
        }
        return new HomePage();
    }


    public static String getDataFromHttpResponse(String url) throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url);
        CloseableHttpResponse response = httpclient.execute(httpget);
        try {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    return EntityUtils.toString(entity);
                } else {
                    throw new ClientProtocolException("Response without entity");
                }
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        } finally {
            response.close();
        }
    }

    public static Integer getIdFromURI(String uri){
        Pattern idPattern = Pattern.compile("\\?id=[0-9]*");
        Matcher matcher = idPattern.matcher(uri);
        while (matcher.find()){
            Integer result = Integer.parseInt(matcher.group().replace("?id=", ""));
            Log.info(String.format("Id [%s] is found in the uri: %s", result, uri));
            return result;
        }
        throw new NoIdFoundException(uri);
    }

    public static String getChannelFromURI(String uri){
        Pattern channelPattern = Pattern.compile("ch=\"?(.+?)\"");
        Matcher matcher = channelPattern.matcher(uri);
        while (matcher.find()){
            String result = matcher.group().replace("ch=", "").replace("\"", "");
            Log.info(String.format("Channel [%s] is found in the uri: %s", result, uri));
            return result;
        }
        throw new NoIdFoundException(uri);
    }





    public static synchronized String getTimeStampNoMillis() {
        String currentTime = String.valueOf(Instant.now().toEpochMilli());
        return currentTime.substring(0, currentTime.length() - 3); //without millis to shorten the string
    }

    public static String getCurrentDateAndTime() {
        ZonedDateTime currentDate = ZonedDateTime.now(ZoneId.systemDefault());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); // TODO: time 'yyyy-MM-dd hh:mm'
        return dtf.format(currentDate);
    }


    public static Process getProcess(String... command) {
        Process process = null;
        try {
            process = new ProcessBuilder(command)
                    .start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

    public static void executeCommand(String commandName, String... command) {
        Process process;
        int exitValue;
        try {
            process = getProcess(command);
            exitValue = process.waitFor();
            System.out.println(commandName + "\n=====\n" + getProcessOutput(process) + "=====\n");
        } catch (InterruptedException e) {
            throw new RuntimeException(commandName, e);
        }
        if (exitValue != 0) {
            System.out.println(getProcessOutput(process));
            throw new RuntimeException(commandName);
        }
    }

    public static String createCurrentTimestamp(){
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MMddHH_mmss");
        return sdf.format(new Date());
    }

    public static void copyFileToServer(String fileName) throws RuntimeException {
        Path filePath = Paths.get("src", "test", "resources", "pages", fileName).toAbsolutePath();
        String command;
        ProcessBuilder processBuilder;
        Process process;
        if (isWindows()) {
            //command = String.format("scp -i combo.key %s ec2-user@18.212.88.58:/var/www/html",//[from 05.11.2019]new ip :
            // 3.93.162.29
            command = String.format("scp -i combo.key %s ec2-user@3.93.162.29:/var/www/html",

                    filePath.toString().replaceAll("\\\\", "/"));
            processBuilder = new ProcessBuilder("cmd", "/c", command);
        } else {
            command = String
                    .format("scp -v -o StrictHostKeyChecking=no -i %s %s ec2-user@172.31.27.116:/var/www/html",
                            System.getProperty("user.home") + "/combo.key",
                            filePath.toString());
            processBuilder = new ProcessBuilder("/bin/sh", "-c", command);
        }
        int exitValue;
        try {
            process = processBuilder.start();
            exitValue = process.waitFor();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("File copy process has failed!", e);
        }
        if (exitValue != 0) {
            System.out.println(getProcessOutput(process));
            throw new RuntimeException("File copy process has failed!");
        }
    }

    private static String getProcessOutput(Process process) {
        int exitValue = process.exitValue();
        StringBuilder sb = new StringBuilder();
        sb.append("File copy process output:").append(System.lineSeparator());
        String line;
        if (exitValue != 0) {
            sb.append("Error message:").append(System.lineSeparator());
            try (BufferedReader stdError
                         = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
                while ((line = stdError.readLine()) != null) {
                    sb.append(line).append(System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try (BufferedReader stdOut
                         = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                while ((line = stdOut.readLine()) != null) {
                    sb.append(line).append(System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sb.append("File copy process exit value: ").append(exitValue).append(System.lineSeparator());
        return sb.toString();
    }

    private static boolean isLinux() {
        String os = System.getProperty("os.name");
        return os.toLowerCase(Locale.ROOT).contains("linux");
    }

    private static boolean isWindows() {
        String os = System.getProperty("os.name");
        return os.toLowerCase(Locale.ROOT).contains("windows");
    }

    public static String getEntityIdFromUrl() {
        String siteUrl = DriverHelper.getDriver().getCurrentUrl();
        String[] splitUrl = siteUrl.split("/");
        return splitUrl[splitUrl.length - 1];
    }

    public static void serialize(Serializable object, File target) throws RuntimeException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(target))) {
            out.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Serialization has failed!", e);
        }
    }

    public static Object deserialize(File target) throws RuntimeException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(target))) {
            return in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Deserialization has failed!", e);
        }
    }

    public static void clearSerializeFile(File target) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(target);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Clearing has failed!", e);
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("The file thread din not close");
            }
        }
    }

    public static synchronized <T> T getRandomValueInList(List<T> someList) {
        List<T> outPutList = someList.parallelStream().collect(Collectors.toList());
        Collections.shuffle(outPutList);
        return outPutList.stream().findAny().get();
    }

    public static synchronized String getUrl() {
        return DriverHelper.getDriver().getCurrentUrl();
    }

    public static int convertStringNumberWithSpacesToInt(String number) {
        number = number.replaceAll(" ", "");
        int num = Integer.valueOf(number).intValue();
        return num;
    }

    /***
     * Normalize String value without coma, space and replaced K to 000.
     * F.E.:
     * 100,000 -> 100000
     * 10K -> 10000
     * 10.1K -> 10100
     * @param value - value from page
     * @return string value
     */
 /*   public static String normalizeStringValueToNumber(String value){
        String zeroAmountForKReplacement = value.contains(".") ? "00" : "000";
        return value
                .replace(" ", "")
                .replace(",", "")
                .replace(".", "")
                .replace("K", zeroAmountForKReplacement);
    }*/

    /**
     * Compare two double value with accuracy
     * @param a value A
     * @param b value B
     * @param accuracy available accuracy
     * @return true if difference between value A and B <= accuracy
     *//*
    public static boolean compareWithAccuracy(double a, double b, int accuracy){
        double different = Math.abs(a-b);
        return different <= accuracy;
    }

    public static int convertStringNumberWithComaAndSpacesToInt(String number) {
        number = number.replaceAll(",", "");
        number = number.replaceAll(" ", "");
        int num = Integer.valueOf(number).intValue();
        return num;
    }

    public static double convertStringNumberWithDollarSymbolToDouble(String number) {
        number = number.replace("$", "");
        double num = Double.valueOf(number).doubleValue();
        return num;
    }

    public static boolean isIntMoreOrEqualsThanZero(int number) {
        return number >= 0;
    }

    public static boolean isDoubleMoreOrEqualsThanZero(double number) {
        return number >= 0.0;
    }

    public static String getMonthName(int monthsAgo) {
        return ZonedDateTime.now().minusMonths(monthsAgo).getMonth().toString();
    }

    public static int getYearByMonthAgo(int mothsAgo) {
        return ZonedDateTime.now().minusMonths(mothsAgo).getYear();
    }

    public static void unGunZipFileInFile(String compressedFilePath, String decompressedFilePath) throws IOException {
        FileInputStream fileIn = null;
        GZIPInputStream gZIPInputStream = null;
        FileOutputStream fileOutputStream = null;
        byte[] buffer = new byte[1024];
        int bytesRead;

        try {
            fileIn = new FileInputStream(compressedFilePath);
            gZIPInputStream = new GZIPInputStream(fileIn);
            fileOutputStream = new FileOutputStream(decompressedFilePath);

            while ((bytesRead = gZIPInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bytesRead);
            }
        } finally {
            gZIPInputStream.close();
            fileOutputStream.close();
            fileIn.close();
        }
    }

    public static void unZipFile(String sourceFilePath, String destinationFolderPath) throws IOException {
        new ZipFile(sourceFilePath).extractAll(destinationFolderPath);
    }

    public static void waitingForTheFileToFinishLoading(Path path, int i, TimeUnit timeUnit) {
        await()
                .atMost(i,timeUnit)
                .until(() -> {
                    return path.toFile().exists();
                });
    }

    public static void removeFile(Path path) throws IOException {
        if (path.toFile().exists()) {
            Files.deleteIfExists(path);
        }
    }

    public static void recursiveDelete(File file) throws IOException {
        if (!file.exists() || !file.toString().contains("Downloads"))
            return;
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                recursiveDelete(f);
            }
        }
        Files.deleteIfExists(Paths.get(file.getPath()));
        //System.out.println("Deleted file or folder: " + file.getAbsolutePath());
    }

    public static void decompress(String in, File out) throws IOException {
        TarArchiveInputStream fin = null;
        GzipCompressorInputStream gzipCompressorInputStream = null;
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(in);
            gzipCompressorInputStream = new GzipCompressorInputStream(fileInputStream);
            fin = new TarArchiveInputStream(gzipCompressorInputStream);
            TarArchiveEntry entry;
            while ((entry = fin.getNextTarEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File curfile = new File(out, entry.getName());
                File parent = curfile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                fileOutputStream = new FileOutputStream(curfile);
                IOUtils.copy(fin, fileOutputStream);
            }
        } finally {
            fin.close();
            gzipCompressorInputStream.close();
            fileInputStream.close();
            fileOutputStream.close();
        }
    }

    public static Map<String, Integer> getMapOfPatformFromFileNamesValuesAndAmountOfRowsForEachOfThem(File fileToParse) throws IOException {
        Map<String, Integer> fileNamesValuesAndAmountOfRowsForEachOfThemMap = new HashMap<>();
        String fileToParseName = fileToParse.toString();
        int amountsOfRowsWithData = 0;
        BufferedReader fileReader = null;
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParseName));
            line = fileReader.readLine();
            while (line != null && !line.equals("")) {
                amountsOfRowsWithData++;
                line = fileReader.readLine();
            }
        } finally {
            fileReader.close();
        }
        fileNamesValuesAndAmountOfRowsForEachOfThemMap.put(fileToParse.getName().split("-")[0], amountsOfRowsWithData - 1);
        return fileNamesValuesAndAmountOfRowsForEachOfThemMap;
    }

    public static List<String> getListOfContentsOfLinesFromTheFile(File fileToParse) throws IOException {
        List<String> listOfContentsOfLinesFromTheFile = new ArrayList<>();
        String fileToParseName = fileToParse.toString();
        BufferedReader fileReader = null;
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParseName));
            do {
                line = fileReader.readLine();
                if (line != null) {
                    listOfContentsOfLinesFromTheFile.add(line);
                }
            } while (line != null && !line.equals(""));
        } finally {
            fileReader.close();

        }
        return listOfContentsOfLinesFromTheFile;
    }

    public static void waitUntilCertainMinuteInHour(int minute) {
        if (new DateTime().getMinuteOfHour() < minute) {
            Utils.delay((minute - new DateTime().getMinuteOfHour()), 0);
        }
    }

    public static String getValueFromClipboard() {
        try {
            return (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (Exception e){
            throw new RuntimeException("An error was occurs while clipboard parsing. " + e.getMessage());
        }
    }

    static class NoIdFoundException extends NullPointerException{
        public NoIdFoundException(String uri) {
            super(String.format("Line {%s} doesn't contains the id", uri));
        }
    }
    */
}
