package utils.listeners;

import org.testng.IMethodInstance;
import org.testng.IMethodInterceptor;
import org.testng.ITestContext;
import utils.Utils;
import utils.annotations.Blocked;
import utils.annotations.DisableOnProduction;
import utils.annotations.DisableOnStaging;

import java.util.List;

public class SkipTestDependsOnBuild implements IMethodInterceptor {

    public List<IMethodInstance> intercept(List<IMethodInstance> list, ITestContext iTestContext) {
        if (Utils.isProductionBuild()) {
            list.removeIf(method -> method.getMethod().getConstructorOrMethod().getMethod().isAnnotationPresent(DisableOnProduction.class)
                    || method.getMethod().getConstructorOrMethod().getMethod().isAnnotationPresent(Blocked.class));
        } else {
            list.removeIf(method -> method.getMethod().getConstructorOrMethod().getMethod().isAnnotationPresent(DisableOnStaging.class)
                    || method.getMethod().getConstructorOrMethod().getMethod().isAnnotationPresent(Blocked.class));
        }
        return list;
    }

}