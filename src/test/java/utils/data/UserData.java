package utils.data;

import org.jdom2.Element;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserData {

    private String username;
    private String password;
    private String userId;

    private String email;

    public UserData(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public UserData(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserData(String username, String name, String password, String userId, String email) {
        this(username, password, email);
        this.userId = userId;
    }

    public UserData(UserData userData) {
        this.username = userData.getUsername();
        this.password = userData.getPassword();
        this.userId = getUserId();
    }

    public UserData(Element element) {
        userId = element.getAttributeValue("userId");
        List<Element> children = element.getChildren();
        for (Element tag : children) {
            switch (tag.getName()) {
                case "username":
                    username = tag.getText();
                    break;
                case "password":
                    password = tag.getText();
                    break;
                case "email":
                    email = tag.getText();
                    break;
                default:
            }
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("ID: %s, Username: %s, Password: %s, Email: %s", userId, username, password, email);
    }

    public String getMailLoginWithoutAlias() {
        Matcher matcher = Pattern.compile(".(\\+[0-9a-zA-Z]+)@").matcher(username);
        return matcher.find() ? username.replace(matcher.group(1), "") : username;
    }
}
