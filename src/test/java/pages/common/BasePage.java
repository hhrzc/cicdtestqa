package pages.common;

import common.Base;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;
import utils.DriverHelper;

public abstract class BasePage extends Base {
    public DriverHelper h;
    public DriverHelper hMinTimeout;
    public DriverHelper hMaxTimeout;
    public DriverHelper hShortTimeout;

    public BasePage() {
        DriverHelper.waitForPageLoad();
        HtmlElementLocatorFactory factory = new HtmlElementLocatorFactory(DriverHelper.getDriver());
        PageFactory.initElements(new HtmlElementDecorator(factory), this);
        h = new DriverHelper(DriverHelper.TIMEOUT_LONG);
        hMinTimeout = new DriverHelper(DriverHelper.TIMEOUT_MINIMAL);
        hMaxTimeout = new DriverHelper(DriverHelper.TIMEOUT_EXTRA_LONG);
        hShortTimeout = new DriverHelper(DriverHelper.TIMEOUT_SHORT);
    }

    @Step("Is the 'Maximum number of reports per minute reached.' alert displayed?")
    public Boolean isMaximumNumberOfReportAlertDisplayed() {
        return hShortTimeout.isAlertDisplayed("Maximum number of reports per minute reached.");
    }

    @Step("Is the 'Maximum number of reports per minute reached.' alert isn't displayed")
    public void checkIfMaximumNumberOfReportAlertHiden() {
        Assert.assertFalse(
                isMaximumNumberOfReportAlertDisplayed(),
                "The msg 'Maximum number of reports per minute reached.' is displayed but should not");
    }

    public abstract boolean isPageDisplayed();
}
