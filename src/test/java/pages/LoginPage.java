package pages;

import common.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.common.BasePage;
import utils.DriverHelper;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//*[@type=\"email\"]")
    WebElement emailLabel;

    @FindBy(xpath = "//*[@name=\"commit\"]")
    WebElement commitBtn;

    @FindBy(xpath = "//*[@label=\"Password\"]")
    WebElement passwordLabel;

    @Override
    public boolean isPageDisplayed() {
        return false;
    }

    public LoginPage writeEmail(String email) {
        h.clearAndTypeIntoInputField(emailLabel, email);
        return this;
    }

    public LoginPage clickButton() {
        h.waitAndClickWebElement(commitBtn);
        return this;
    }

    public LoginPage writePassword(String pass) {
        h.clearAndTypeIntoInputField(passwordLabel, pass);
        return this;
    }

    public boolean waitForPageLoad() {
        DriverHelper.waitForPageLoad();
        return h.isElementPresent(commitBtn);
    }

}
